# Britecore Engineering Application

By: Ricardo Molina Sendra

Email: ricardo.molina.sendra@gmail.com

Web site: www.ricardomol.com

Deployed version of the project: http://46.101.25.164:8000/

Project's Github: https://github.com/Ricardomol/dynamic-fields-app-api

Project's Gitlab: https://gitlab.com/ricardo.molina.sendra/dynamic-fields-app-api

---

## Run locally

Just clone the repo and run `$docker-compose up --build`.

When everything is ready, point your browser to `http://localhost:8000/`.

To access Django's Admin:
U: `admin`
P: `DogCat77`

## Overview

This SPA has 3 main sections: **Create**, **List** and **About**.

The **Create** section displays a list of `RiskType`s.
Each `RiskType` is represented by a form that the user can fill in and submit.
The fields that compose a certain `RiskType` are defined in a specific JSON format.
For example, the fields of a `RiskType` called "Real Estate", would be defined like this:

```json
{
    "label": "Area",
    "field_id": 10,
    "field_name": "area",
    "field_type": "Integer",
    "placeholder": "Enter the area of the real estate"
},
{
    "label": "Units",
    "multi": false,
    "options": [
        "Squared Kilometres",
        "Squared Metres",
        "Squared Yards",
        "Squared Feet"
    ],
    "field_id": 11,
    "field_name": "units",
    "field_type": "SelectList",
    "placeholder": ""
},
{
    "label": "Address",
    "field_id": 12,
    "field_name": "address",
    "field_type": "TextArea",
    "placeholder": "Enter full address"
},
{
    "label": "Price",
    "field_id": 13,
    "field_name": "price",
    "field_type": "Float",
    "placeholder": "Enter price"
},
```

If you are running the project locally, this data should be created manually
through the Django Admin [ http://localhost:8000/admin/core/risktype/ ]. Ideally,
this initial data would be inserted into the database via a **fixture**.

The **List** section displays a list of the `Risk`s that have been created when
submitting the forms listed in the aforementioned **Create** section.

## API Endpoints

The **API endpoint** that retrieves the list of `RiskType`s is located at:
http://46.101.25.164:8000/api/risktypes/

The **API endpoint** that retrieves the list of `Risk`s is located at:
http://46.101.25.164:8000/api/risks/

## Project structure

The project has been divided into **3 different Django apps**: `core`, `risks` and `frontend`.

- The `core` app holds all of the central code that is important to the rest of the
  apps in our system. For example, it is in charge of the database, migrations, etc.

- The `risks` app is responsible for the specific related to the definition and management
  of the `RiskType` and `Risk` classes.

- The `frontend` holds the Vue.js app.

## Approach

To solve the problem I have come up with 2 main classes: `RiskType` and `Risk`
(defined in `app/core/models.py`). Both of these classes rely heavily
on **Postgres' JSON data types** [ https://www.postgresql.org/docs/devel/datatype-json.html ].

### The `RiskType` class

The `RiskType` class defines the different kinds of risk **types** that can exist.
Examples of risk types are: automobile policies, cyber liability coverage,
prize insurances, etc.

This class has the following attributes:

- `name`:
  - Type: CharField.
  - Stores the name of the risk type.
  - Example: "Automobile Policy"
- `data`:
  - Type: JSONField.
  - Stores in JSON format the fields that compose the risk type.
  - Example:
  ```json
  [
    {
      "value": 1,
      "field_id": 0,
      "field_name": "risk_type_id",
      "field_type": "Hidden"
    },
    {
      "label": "Name",
      "field_id": 1,
      "field_name": "risk_name",
      "field_type": "TextLine",
      "placeholder": "Enter name"
    },
    {
      "label": "Brand",
      "field_id": 2,
      "field_name": "brand",
      "field_type": "Text",
      "placeholder": "Enter brand name"
    },
    {
      "label": "Model",
      "field_id": 3,
      "field_name": "model",
      "field_type": "Text",
      "placeholder": "Enter model"
    },
    {
      "label": "Currency",
      "multi": false,
      "options": ["$", "€", "¥", "Ƀ"],
      "field_id": 4,
      "field_name": "currency",
      "field_type": "SelectList",
      "placeholder": ""
    },
    {
      "label": "Price",
      "field_id": 5,
      "field_name": "price",
      "field_type": "Float",
      "placeholder": "Enter price"
    },
    ...
  ]
  ```

### The `Risk` class

The `Risk` class represents each specific risk and the values it holds.
For example: John Doe's car policy.

This class has the following attributes:

- `name`:
  - Type: CharField.
  - Stores the name of the risk.
  - Example: "John Doe's Automobile Policy"
- `risk_type`:
  - Type: ForeignKey(Risk).
  - Points to the Risk which this risk belongs to.
  - Example: The risk with name "John's Car Policy" points to the RiskType with
    `id = 1` named "Automobile Policy".
- `data`:
  - Type: JSONField.
  - Stores, in JSON format, the values of the fields that correspond to this risk
    according to its RiskType.
  - Example:
  ```json
  [
    { "field_id": "0", "field_value": "1" },
    { "field_id": "1", "field_value": "John's car policy" },
    { "field_id": "1", "field_value": "BMW" },
    { "field_id": "4", "field_value": "$" },
    { "field_id": "5", "field_value": "99.9" },
    { "field_id": "6", "field_value": "on" },
    { "field_id": "7", "field_value": "2019-01-01" }
  ]
  ```

### Putting it all together

Let's look at the above examples to see how everything is linked together.

If we inspect the `Risk` defined above, the `risk_type` field points to the
`RiskType` table, thus, informing us that the risk is of type
`Automobile Policy`, for example.

The `RiskType.data` field, lets us know the value of each field defined
in the corresponding `RiskType`, which in this case, as we said, is "Automobile Policy".

So, we go and check the fields that compose the `Risk` called "Automobile Policy":

```
- Risk name:

  - Id: 1
  - Name: "risk_name"
  - Type: "TextLine"

- Brand:

  - Id: 2
  - Name: "brand"
  - Type: "Text"

- Model

  - Id: 3
  - Name: "model"
  - Type: "Text"

- Currency:

  - "multi": false
  - "options": ["$", "\u20ac", "\u00a3", "\u00a5", "\u0243"]
  - Id: 4
  - Name: "currency"
  - Type: "SelectList"

- Price:

  - Id: 5,
  - Name: "price"
  - Type: "Float"

- Is Active:

  - Id: 6,
  - Name: "is_active"
  - Type: "Boolean"

- Purchase date:
  - Id: 7,
  - Name: "purchased"
  - Type: "Date"
```

We link both pieces of information together by matching the `field_id` values:

```
- Risk Name:

  - Id: 1
  - Type: "Text"
```

```
{ "field_id": "1", "field_value": "John's car policy" }
```

Therefore, the name of the `Risk` is ""John's car policy".

Applying the same logic:

```
- Brand:

  - Id: 2
  - Type: "TextLine"
```

```
{ "field_id": "2", "field_value": "BMW" }
```

Therefore, the `brand` of the `Risk` is "BMW".

## Frontend

As we have seen above, each field that we define via JSONField in our `RiskType`'s
has a field called `field_type`.

Example:

```json
    {
      "label": "Name",
      "field_id": 1,
      "field_name": "risk_name",
      "field_type": "TextLine",  # <---
      "placeholder": "Enter name"
    },
```

The possible values for the `field_type` field are:

- `Boolean`
- `Date`
- `Float`
- `Integer`
- `SelectList`
- `TextLine`
- `TextArea`
- `Hidden`

Each of these corresponds to a different Vue component that will be responsible
for rendering the corresponding widget. They can all be found under the
`app/frontend/src/components/FormFields` folder.

In `app/frontend/src/views` there are define 4 view components. Each will
represent a different "screen":

- `Home`. Screen to be displayed when listing the different kinds of risk types.
- `List`. Screen to be displayed when listing the submitted forms.
- `About`. Screen to display the "About" page.
- `Not Found`. Screen to be displayed when an incorrect URL has been typed.

The whole application is embedded in a main component called `App`.

## Deployment method

The application has been deployed in a **Digital Ocean Droplet**.

For this purpose, a Gitlab repo has been configured to utilize the **Continuous
Integration and Deployment** pipeline. This pipeline consists of 3 stages:

1. Test (run django's tests and flake8)
2. Build (build the docker image)
3. Deploy (run `docker-compose up` in the server)

Thus, any commit pushed to `master` or `develop` branches will be automatically
tested, built and deployed if no problem is encountered.

## Issues / TODO's

- The application is currently being served by Django's web server.
  Django's server is just not designed for production environments.
  **Nginx** and **Gunicorn** should be used instead.

- A **fixture** should be created in order to feed the database with the initial
  data representing some `RiskType`s to work with.

- The responsiveness of the design can be improved.

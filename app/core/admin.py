from django.contrib import admin
from .models import RiskType, Risk


class RiskTypeAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display_ = ['name', 'data']


class RiskAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display_ = ['name', 'data', 'risk_type']


admin.site.register(RiskType, RiskTypeAdmin)
admin.site.register(Risk, RiskAdmin)

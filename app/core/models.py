from django.db import models
from django.contrib.postgres.fields import JSONField


class RiskType(models.Model):
    objects = models.Manager()
    name = models.CharField(max_length=255, default="", unique=True)
    data = JSONField(db_index=True, default=dict, blank=True)

    def __str__(self):
        return self.name


class Risk(models.Model):
    objects = models.Manager()
    name = models.CharField(max_length=255, default="", unique=True)
    data = JSONField(db_index=True, default=dict, blank=True)
    risk_type = models.ForeignKey(RiskType, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

#!/bin/bash

./manage.py collectstatic --noinput
# Migration files are commited to git. Makemigrations is not needed.
# ./manage.py makemigrations app_name
./manage.py migrate
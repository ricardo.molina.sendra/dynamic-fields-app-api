from django.test import TestCase
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient

from core.models import RiskType
from risks.serializers import RiskTypeSerializer


CREATE_RISK_URL = reverse("risks:risktype-list")
LIST_RISKS_URL = reverse("risks:risktype-list")

PAYLOAD = {
    "name": "Automobile",
    "data":
    [
        {
            "field_name": "brand",
            "field_type": "string",
        },
        {
            "field_name": "model",
            "field_type": "string",
        },
        {
            "field_name": "price",
            "field_type": "float",
        },
        {
            "field_name": "currency",
            "field_type": "string",
        },
        {
            "field_name": "currency",
            "field_type": "string",
        },
        {
            "field_name": "date_purchased",
            "field_type": "datetime",
        },
    ],
}


def detail_url(risk_type_id):
    return reverse("risks:risktype-detail", args=[risk_type_id])


class PublicRiskAPITests(TestCase):
    """Test the risks API (public)"""

    def setUp(self):
        self.client = APIClient()

    def test_risk_type_string(self):
        """Test the risk type strin grepresentation"""
        risk_type = RiskType.objects.create(
            name="Sample Risk Type",
            data={"field_name": "A", "field_value": "1"}
        )
        self.assertEqual(str(risk_type), risk_type.name)

    def test_create_valid_risk_type_success(self):
        """Test creating risk type with valid payload is successful"""
        res = self.client.post(CREATE_RISK_URL, PAYLOAD, format="json")
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_risk_type_exists(self):
        """Test creating a risk type that already exists"""
        RiskType(**PAYLOAD).save()
        res = self.client.post(CREATE_RISK_URL, PAYLOAD, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_risk_type(self):
        """Test retrieving the list of risk types"""
        RiskType(name="RT1", data={"fn": "a", "fv": 1}).save()
        RiskType(name="RT2", data={"fn": "b", "fv": 2}).save()

        res = self.client.get(LIST_RISKS_URL)

        risk_types = RiskType.objects.all().order_by('-name')
        serializer = RiskTypeSerializer(risk_types, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_view_risk_type_detail(self):
        """Test viewing a risk type detail"""
        rt = RiskType.objects.create(
            name="RT1",
            data={"fn": "a", "fv": 1}
        )

        url = detail_url(rt.id)
        res = self.client.get(url)

        serializer = RiskTypeSerializer(rt)
        self.assertEqual(res.data, serializer.data)

from core.models import RiskType, Risk
from rest_framework import serializers


class RiskTypeSerializer(serializers.ModelSerializer):
    """Serializer for the RiskType object"""

    class Meta:
        model = RiskType
        fields = ('name', 'data')

    def create(self, validated_data):
        """Create a new risk type with its JSON data and return it"""
        return RiskType.objects.create(**validated_data)


class RiskSerializer(serializers.ModelSerializer):
    """Serializer for the Risk object"""

    class Meta:
        model = Risk
        fields = ('name', 'data', 'risk_type')

    def create(self, validated_data):
        """Create a new risk with its JSON data and return it"""
        return Risk.objects.create(**validated_data)

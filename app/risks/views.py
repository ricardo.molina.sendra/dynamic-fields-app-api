from rest_framework import viewsets, mixins
from rest_framework.response import Response
from .serializers import RiskTypeSerializer, RiskSerializer
from core.models import RiskType, Risk


class RiskTypeViewSet(viewsets.ModelViewSet, mixins.ListModelMixin):
    """Manage risk types in the database"""
    queryset = RiskType.objects.all()
    serializer_class = RiskTypeSerializer

    def get_queryset(self):
        """Return objects for the current authenticated user only"""
        return self.queryset.order_by("-name")


class RiskViewSet(viewsets.ModelViewSet, mixins.ListModelMixin):
    """Manage risks in the database"""
    queryset = Risk.objects.all()
    serializer_class = RiskSerializer

    def get_queryset(self):
        """Return list of risks"""
        return self.queryset.order_by("-name")

    def list(self, request, *args, **kwargs):
        all_risks = Risk.objects.all().order_by('-id')

        result = []

        for risk in all_risks:
            risk_result = {
                "risk_type_name": risk.risk_type.name,
                "name": risk.name,
                "data": risk.data,
            }

            for idx, risk_field in enumerate(risk.data):
                for risk_type_field in risk.risk_type.data:
                    risk_type_field_id = int(risk_type_field["field_id"])
                    risk_field_id = int(risk_field["field_id"])

                    if risk_type_field_id == risk_field_id:
                        risk_result["data"][idx].update(risk_type_field)
                        break

            result.append(risk_result)

        return Response(result)

from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('risktypes', views.RiskTypeViewSet)
router.register('risks', views.RiskViewSet)

app_name = "risks"

urlpatterns = [
    path("", include(router.urls)),
]

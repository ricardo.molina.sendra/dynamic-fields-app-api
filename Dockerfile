FROM python:3.7-alpine
MAINTAINER Ricardo Molina Sendra

ENV PYTHONUNBUFFERED 1

RUN mkdir /app

# make the 'app' folder the current working directory
WORKDIR /app

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY ./app .

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client
RUN apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers postgresql-dev
RUN pip install -r /requirements.txt
RUN apk del .tmp-build-deps

# copy both 'package.json' and 'package-lock.json' (if available)
COPY app/frontend/package*.json ./frontend/

# Install npm
RUN  apk add --update nodejs && apk add --update nodejs-npm

# install project dependencies
WORKDIR /app/frontend
RUN npm install

# build app for production with minification
RUN npm run build

RUN adduser -D user
USER user

WORKDIR /app
CMD ["sh scripts/entrypoint.sh"]

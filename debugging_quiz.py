from cryptography.fernet import Fernet

key = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='

# Oh no! The code is going over the edge! What are you going to do?
message = b'gAAAAABcuG2DDAjLaG0eZZ4cur_FhmXftk8VvyHG4K4TSh09AN2mXbcd8PuZxVaokdoGuZYJJZMJRycaQw0v1q1q2OYsBDzhMSGP1UGvVLV7LjpekgBuEIn04EP9s4x-elu1i0YFV7IK-WKZlBpuvyacMyH_z3yIV-FNMxvGyrLhSNKvr4Tc2Bo='

def main():
    f = Fernet(key)
    print(f.decrypt(message))


if __name__ == "__main__":
    main()
